@extends('layout.raisitepu')
@section('contents')
    <div class="panel-heading ">
        <h3 class="panel-title">Selamat Datang {{ Auth::user()->name }}
        </h3>
    </div>
    <div class="panel-body">
        <form role="form" method="POST" action="/absensi">
            <fieldset>
                {{ csrf_field() }}
                <input type="hidden" name="users_id" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value="Hadir">Hadir</option>
                        <option value="Izin">Izin</option>
                        <option value="Sakit">Sakit</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" placeholder="Keterangan" name="keterangan" autofocus></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Absen</button>
                <a href="/logout" class="btn btn-danger">Logout</a>
            </fieldset>
        </form>
    </div>
@endsection
